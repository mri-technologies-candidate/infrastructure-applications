terraform {
  required_version = "1.4.5"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.88.0"
    }
  }
}

provider "google" {
  project = "turing-bebop-383619"
  region  = "us-west1-b"
}

resource "google_container_cluster" "cluster" {
  name               = "my-gke-cluster"
  location           = "us-central1-a"
  initial_node_count = 1
  node_config {
    machine_type = "e2-medium"
  }
}

